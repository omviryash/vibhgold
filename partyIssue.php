<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['ok'])) {
    unset($_POST['ok']);
	
	/*if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { 
		// Updating the transaction record
	} else {
			// insert code for all text boxes in item table
					
			$sSQL = "INSERT INTO item (item_name,item_type_id,category_id,is_special)
											 VALUES ('$item_name','$item_type','$category','$special')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		} 
	mysqli_close($conn);
    header("Location:itemMaster.php");
    exit;*/
}

/*if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master where transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}*/
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Party Issue</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">Party Issue</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body table-responsive">
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Account</label>
									</div>
									<div class="form-group col-lg-3">
										<select class="form-control" name="account" id="account">
											<option value="">Select Account</option>
											<?php  	$sSQL = "SELECT * from account where account_status = 'A' ORDER BY first_name";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option value="<?php echo $row['account_id']; ?>"><?php echo $row['first_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-2">
										<label>Date</label>
									</div>
									<div class="form-group col-md-3">
                                        <div class="input-group">
                                            <select name="fromDate" id="currentDate" class="form-group pull-left">
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <?php if($i < 10){ $i = '0'.$i;} ?>
                                                    <?php if(date('d') == $i) { ?>
                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            <select name="fromMonth" id="currentMonth" class="form-group pull-left">
                                              <?php for($i=1;$i<=12;$i++){?>
                                                  <?php if($i < 10){ $i = '0'.$i;}?>
                                                  <?php if(date('m') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                            <select name="fromYear" id="currentYear" class="form-group pull-left">
                                              <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                  <?php if(date('Y') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                        </div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Item Name</label>
									</div>
									<div class="form-group col-lg-3">
										<select class="form-control" name="category" id="category">
											<option value="">Select Item</option>
											<?php  	$sSQL = "SELECT * from item ORDER BY item_name";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option value="<?php echo $row['item_id']; ?>"><?php echo $row['item_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-2">
										<label>Category</label>
									</div>
									<div class="form-group col-lg-3">
										<select class="form-control" name="category" id="category">
											<option value="">Select Item Category</option>
											<?php  	$sSQL = "SELECT * from category ORDER BY category";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category']; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Gross Weight</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="gross_weight" id="gross_weight" class="form-control"> 
									</div>
								</div>
								    <!-- Table Display-->
								<table id="partyLedgerList" class="table table-bordered">
									<thead class="multiple_header">
										<tr>
											<!--<th>Action</th>-->
											<th>Serial No</th>
											<th>MM Name</th>
											<th>Pieces</th>
											<th>Weight / Pieces</th>
											<th>Total Weight</th>
											<th>Rate / Pieces</th>
											<th>Total Rupees</th>
										</tr>
										<tr>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
											<th style="text-align:center">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
											<tr>
												<td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
											</tr>
									</tbody>
									<tfoot>
										<tr class="oddRow">
											<th style="text-align: right;"><?php echo ""; ?></th>
											<th style="text-align: right;">Total</th>
											<th style="text-align: right;">0</th>
											<th style="text-align: right;"><?php echo ""; ?></th>
											<th style="text-align: right;">0</th>
											<th style="text-align: right;"><?php echo ""; ?></th>
											<th style="text-align: right;">0</th>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="box-footer">
								<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
								<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='itemMaster.php'"/>						
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
</body>
</html>