<?php
	include_once('includes/header.php');
	include_once('includes/topheader.php');
	include_once('includes/leftside.php');
		
?>
<aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Purchase Multi Delete
                       <!-- <small>it all starts here</small>-->
                    </h1>
                    <!--<ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>-->
                </section>

                <!-- Main content -->
                <section class="content">
                 <div class="row">
					 <!-- left column -->
						<?php include_once('msg.php');?>
                        <div class="col-md-8">
							<div class="box box-primary">
								<form action="" method="post">
									<div class="box-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
												<label>Party</label>
												<select class="form-control" name="party" id="party">
													<option value="1">Vikas</option>
													<option value="2">Amit</option>
												</select>
												</div>
												<div class="col-md-6">
													<label>Current Balance:</label>
													<br/>
													<span id="getfineamt"></span>
												</div>
											</div>
										</div>
										<form class="form-group">
											<div class="row">
											<div class="col-md-4">
												<label>From Date</label>											
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" name="fromdate" id="fromdate" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                       			</div>
											</div>
											<div class="col-md-4">
												<label>To Date</label>
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													<input type="text" name="todate" id="todate" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                       			</div>
											</div>
											<div class="col-md-4">
												<label>&nbsp;</label>
												<div class="form-group">
													<input type="submit" name="show" value="Show" class="btn btn-primary btn-flat"/>
												</div>
											</div>
										</div>
										</form>
									</div>
								</form>
								
								<!-- Table Display-->
								<form action="" method="post">
							<div class="box-body">
								<table class="table table-bordered">
                                        <tbody>
											<tr>
												<th>&nbsp;</th>
												<!--<th>&nbsp;</th>-->
												<th>Date</th>
												<th>Gross Wt.</th>
												<th>Copper</th>												
												<th>Net Wt.</th>
												<th>Fine Gold</th>												
												<th>Amount</th>
												<th>Comments</th>
											</tr>
											<tr>
												<td>
													<div class="checkbox">                                                
														<input type="checkbox" name="chkdel[]"/>                                                                                                      
													</div>
												</td>
												<!--<td><a href="#">Edit</a> | <a href="#">Delete</a></td>-->
												<td>10-12-2013</td>
												<td>200</td>
												<td>30</td>
												<td>400</td>
												<td>50</td>
												<td>1000</td>
												<td>It is good</td>
											</tr>
											<tr>
												<td>
													<div class="checkbox">                                                
														<input type="checkbox" name="chkdel[]"/>                                                                                                      
													</div>
												</td>
												<!--<td><a href="#">Edit</a> | <a href="#">Delete</a></td>-->
												<td>10-12-2013</td>
												<td>300</td>
												<td>40</td>
												<td>500</td>
												<td>60</td>
												<td>2000</td>
												<td>It is excellent</td>
											</tr>		
										</tbody>
									</table>
									<div class="form-group">
										<label><a href="#">Select All</a> / <a href="#">Clear All</a></label>
									</div>

							</div>
							<div class="box-footer">
                                        <input type="submit" name="delete" value="Delete" class="btn btn-primary"/>
                                 </div>
							</form>
							</div>
						</div>
				</div>
			</section><!-- /.content -->
    </aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">            
			$(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
				$("[data-mask]").inputmask();
				});
			$("#party").change(function(){
			var getValue=$("#party").val();
			
				if(getValue==1)
				{
					var ans ="Fine: 30"+"&nbsp;&nbsp;"+"Amount: 500";
				}
				else
				{
					var ans ="Fine: 60"+"&nbsp;&nbsp;"+"Amount: 1000";
				}
				$("#getfineamt").html(ans);
			});
			
</script>
