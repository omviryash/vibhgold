<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['ok'])) {
    unset($_POST['ok']);
	
	/*if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { 
		// Updating the transaction record
	} else {
			// insert code for all text boxes in item table
					
			$sSQL = "INSERT INTO item (item_name,item_type_id,category_id,is_special)
											 VALUES ('$item_name','$item_type','$category','$special')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		} 
	mysqli_close($conn);
    header("Location:itemMaster.php");
    exit;*/
}

/*if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master where transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}*/
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>MM Book</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">MM Item Master</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-2">
										<label>MM Name</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="mm_name" id="mm_name" class="form-control"> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Opening Pieces</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="opening_pieces" id="opening_pieces" class="form-control"> 
									</div>
									<div class="form-group col-lg-2">
										<select class="form-control" name="p_crdr" id="p_crdr">
											<option value="CR">Credit</option>
											<option value="DR">Debit</option>
										</select> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Opening Weight</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="opening_weight" id="opening_weight" class="form-control"> 
									</div>
									<div class="form-group col-lg-2">
										<select class="form-control" name="w_crdr" id="w_crdr">
											<option value="CR">Credit</option>
											<option value="DR">Debit</option>
										</select> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Is Special ?</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="checkbox" name="special" id="special" class="form-control" value="Y"> 
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
								<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='itemMaster.php'"/>						
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
</body>
</html>