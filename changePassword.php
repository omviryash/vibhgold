<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

global $dml;

$sSQL = "SELECT fullName,password FROM user WHERE userId = ".$_SESSION["logged_user_id"]."";
$rs = mysqli_query($dml->conn, $sSQL) or print(mysqli_error());
	if(mysqli_num_rows($rs) > 0){
		$row = mysqli_fetch_assoc($rs);
        }

?>
<style type="text/css">
.user-entry .form-group { margin-bottom:8px; }
.user-entry .form-control { height:22px; padding:0px 5px; font-size:13px;}
.fltLeft label { float:left; width:100px; line-height: 1.42857;}
.fltLeft .form-control { width:70%; }

.box {margin-bottom:10px; }
.box .box-body { padding: 5px; }
.form-group { margin-bottom:0px; line-height:0;}
.form-control { height:23px; padding:0px 5px; font-size:13px;}
.ps-entry textarea.form-control { height:auto;}
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { vertical-align:middle; padding: 4px 8px; }
label {margin-bottom:0px;}
.pagination > li > a, .pagination > li > span { padding: 0 12px; }
table.dataTable { margin-bottom: 0px !important; margin-top: 0px !important;}
.box .box-footer { margin-left:100px; padding:0px;}
.btn { padding: 0 5px; }
.form-control:focus { border-color:red !important; }
.fltLeft label {
    float: left;
    line-height: 1.42857;
    width: 120px;
}
</style>
<aside class="right-side strech">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Change Password</h1>
</section>
<section class="content user-entry">
	<div class="row">
    <?php include_once('msg.php');
    if (isset($_GET["msg"]) && $_GET["msg"] != "") {
    ?>
            
            <div class="alert alert-danger alert-dismissable">
  <i class="fa fa-ban"></i>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
  <b>Error!</b> 
	<?php  echo $_GET["msg"];
?>
</div>
          <?php }  ?>
            
    <div class="col-xs-12">
        <form action="dochangepassword.php" autocomplete="off" method="post" name="changepassword" id="changepassword">
        <input type="hidden" name="frmPosted" value="1">
        <input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION["logged_user_id"];?>">
      <input type="hidden" name="username" id="username" value="<?php echo $_SESSION["logged_username"];?>">
        <div class="col-md-6 fltLeft">
          <div class="box">
            <div class="box-body">
            	<div class="form-group">
                <label>Old Password</label>
                <input type="password" name="oldpassword" id="oldpassword" class="form-control" autocomplete="off">
              </div>
              <div class="form-group">
                <label>New Password</label>
                <input type="password" name="newpassword" id="newpassword" class="form-control" autocomplete="off">
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
               <input type="password" name="confpassword" id="confpassword" class="form-control" autocomplete="off">
              </div>
                 
              <div class="box-footer">
                <input type="submit" name="ok" value="Submit" class="btn btn-primary"/>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>changepassword.js" type="text/javascript"></script>
</body></html>