<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");
 
$fieldForParty=array('party_id','name', 'current_fine_gold', 'current_finecrdr', 'current_amount', 'current_amountcrdr');
$whereForParty='';
$orderbyForParty='party_id';
$orderForParty='ASC';
$getParty=$dml->selectWithNestedKey('party',$fieldForParty,$whereForParty,$orderbyForParty,$orderForParty);

$fieldForItem=array('item_id','item_name');
$whereForItem='';
$orderbyForItem='item_id';
$orderForItem='ASC';
$getItem=$dml->selectWithNestedKey('item',$fieldForItem,$whereForItem,$orderbyForItem,$orderForItem);

if(isset($_POST['ok']))
{
    unset($_POST['ok']);
    unset($_POST['item_id']);
    unset($_POST['gross_weight']);
    unset($_POST['copper']);
    unset($_POST['net_weight']);
    unset($_POST['touch']);
    unset($_POST['waste']);
    unset($_POST['fine_gold']);
    unset($_POST['amount']);
    $_POST['created_at'] = date('Y-m-d H:i:s');
    $_POST['updated_at'] = date('Y-m-d H:i:s');
    $_POST['purchase_date'] = $_POST['currentYear'] . '-' . $_POST['currentMonth'] . '-' . $_POST['currentDate'];
    
    unset($_POST['currentYear']);
    unset($_POST['currentMonth']);
    unset($_POST['currentDate']);
    
    if(isset($_POST['picture']) && $_POST['picture'] == "")
        $_POST['picture'] = "";
    else{
        // development remaining
        
        move_uploaded_file($_FILES["picture"]["tmp_name"], "upload/purchase/" . $_FILES["picture"]["name"]);
        
        $_POST['picture'] = "upload/purchase/" . $_FILES["picture"]["name"];
    }

    // First Insert Content into purchase table
    $purchase_table_data_insert = array('party_id' => $_POST['party_id'], 'tot_gross_weight' => $_POST['tot_gross_weight'], 'tot_copper' => $_POST['tot_copper'], 'tot_net_weight' => $_POST['tot_net_weight'], 'tot_fine_gold' => $_POST['tot_fine_gold'], 'tot_amount' => $_POST['tot_amount'], 'purchase_date' => $_POST['purchase_date'], 'comments' => $_POST['comment'], 'picture' => $_POST['picture'], 'created_at' => $_POST['created_at'], 'updated_at' => $_POST['updated_at']);
  
    $purchase_id = $dml->simpleInsert('purchase', $purchase_table_data_insert);
    
    // Process for purchasedetail table
    $countRow = count($_POST['purchasedetail']['item_id']);
    
    for($i = 0; $i < $countRow; $i++){
        
        $purchasedetail_table_data_insert = array('purchase_id' => 1, 'item_id' => $_POST['purchasedetail']['item_id'][$i], 'gross_weight' => $_POST['purchasedetail']['gross_weight'][$i], 'copper' => $_POST['purchasedetail']['copper'][$i], 'net_weight' => $_POST['purchasedetail']['net_weight'][$i], 'touch' => $_POST['purchasedetail']['touch'][$i], 'waste' => $_POST['purchasedetail']['waste'][$i], 'fine_gold' => $_POST['purchasedetail']['fine_gold'][$i], 'amount' => $_POST['purchasedetail']['amount'][$i], 'created_at' => $_POST['created_at'], 'updated_at' => $_POST['updated_at']);
        
        $dml->simpleInsert('purchasedetail', $purchasedetail_table_data_insert);
        
    }
    
    $_SESSION['success'] = "Record is inserted.";
    header("Location:entryPurchase.php");
    exit;
}

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
  
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Purchase
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <form name="purchaseEntryForm" id="purchaseEntryForm" action="" method="post" enctype="multipart/form-data">
     <div class="row">
        <!-- left column -->
        <?php include_once('msg.php');?>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-lg-3">
                            <label>Date </label>
                            <div class="input-group">
                                <select name="currentDate" id="currentDate" class="form-group pull-left">
                                    <?php for($i=1;$i<=31;$i++){?>
                                        <?php if($i < 10){ $i = '0'.$i;} ?>
                                        <?php if(date('d') == $i) { ?>
                                            <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php } ?>
                                    <?php }?>
                                  </select>
                                  <select name="currentMonth" id="currentMonth" class="form-group pull-left">
                                    <?php for($i=1;$i<=12;$i++){?>
                                        <?php if($i < 10){ $i = '0'.$i;}?>
                                        <?php if(date('m') == $i) { ?>
                                            <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php } ?>
                                    <?php }?>
                                  </select>
                                  <select name="currentYear" id="currentYear" class="form-group pull-left">
                                    <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                        <?php if(date('Y') == $i) { ?>
                                            <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php } ?>
                                    <?php }?>
                                  </select>

                            </div>
                        </div>
                        <div class="form-group col-lg-5">
                            <label>Party</label>                        
                            <select class="form-control" name="party_id" id="party_id">
                                <option value="" data-total-fine="0.000 Dr" data-total-amount="0.00 Dr">Select party</option>
                                <?php if(count($getParty)>0){
                                foreach ($getParty as $k=>$v)
                                {
                                    $finedrcr = "";
                                    if($v['current_finecrdr'] == 'debit')
                                        $finedrcr = ' Dr';
                                    else if($v['current_finecrdr'] == 'credit')
                                        $finedrcr = ' Cr';
                                    
                                    $amountdrcr = "";
                                    if($v['current_amountcrdr'] == 'debit')
                                        $amountdrcr = ' Dr';
                                    else if($v['current_amountcrdr'] == 'credit')
                                        $amountdrcr = ' Cr';
                                    
                                    $current_fine_gold = number_format((float)$v['current_fine_gold'], 3) . $finedrcr;
                                    $current_amount = number_format((float)$v['current_amount'], 2) . $amountdrcr;
                                ?>
                                    <option value="<?php echo $v['party_id'];?>" data-total-fine="<?php echo $current_fine_gold; ?>" data-total-amount="<?php echo $current_amount; ?>" >
                                        <?php echo $v['name'];?>
                                    </option>
                                  <?php 
                                 }
                                 }?>
                            </select>
                        </div>
                        <div class="form-group col-lg-3">
                            <label>Current Balance:</label>
                            <br/>
                            <div id="currentBalanceContainer" class="label-warning">
                                <div id="fineGoldContent">
                                    <label>Fine Gold (Grm):</label>
                                    <span id="getfine">0.000 Dr</span>
                                </div>
                                <div id="amountContent">
                                    <label>Amount (Rs.):</label>
                                    <span id="getamt">0.00 Dr</span>
                                </div>
                            </div>
                        </div>
                     </div>

                    <div class="row">&nbsp;</div>

                    <div id="itemEntry" class="row">
                        <div class="form-group col-lg-3">									
                            <label>Item </label>                                      
                            <select class="form-control" name="item_id" id="item_id">
                                <option value="">Select item</option>
                                <?php if(count($getItem)>0){
                                foreach ($getItem as $k=>$v)
                                {
                                ?>                          
                                <option value="<?php echo $v['item_id'];?>"><?php echo $v['item_name'];?></option>													
                                <?php 
                               }
                               }?>
                            </select>             
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Gross Wt.</label>
                            <input type="text" name="gross_weight" id="gross_weight" class="form-control">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Copper</label>
                            <input type="text" name="copper" id="copper" class="form-control">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Net Wt.</label>
                            <input type="text" name="net_weight" id="net_weight" class="form-control" readonly tabindex="-1">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Touch(%)</label>
                            <input type="text" name="touch" id="touch" class="form-control">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Waste(%)</label>
                            <input type="text" name="waste" id="waste" class="form-control">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Fine Gold</label>
                            <input type="text" name="fine_gold" id="fine_gold" class="form-control" readonly tabindex="-1">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>Amount</label>
                            <input type="text" name="amount" id="amount" class="form-control">
                        </div>

                        <div class="form-group col-lg-1">
                            <label>&nbsp;</label>
                            <input type="button" id="additem" name="add" value="Add" class="form-control btn btn-primary"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Display Table View-->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Purchase View</h3>
                </div>
                <div class="box-body">
                    <table id="purchase_list" class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width:10%">&nbsp;</th>
                                <th>Item</th>
                                <th>Gross Wt.</th>
                                <th>Copper</th>
                                <th>Net Wt.</th>
                                <th>Touch(%)</th>
                                <th>Waste(%)</th>
                                <th>Fine Gold</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>					
        </div>
    </div>

    <!-- Control -->
    <div class="row">
    <!-- left column -->
        <div class="col-md-7">
            <!-- general form elements -->
            <div class="box box-primary">
                <!--<div class="box-header">
                    <h3 class="box-title">Add New Category</h3>
                </div>--><!-- /.box-header -->
                <!-- form start -->
                    <div class="box-body">
                        
                        <div class="row">
                            <div class="form-group col-lg-2">
                                <label>Gross Wt.</label>
                                <input type="text" name="tot_gross_weight" id="tot_gross_weight" class="form-control" readonly tabindex="-1">
                            </div>

                            <div class="form-group col-lg-2">
                                <label>Copper</label>
                                <input type="text" name="tot_copper" id="tot_copper" class="form-control" readonly tabindex="-1">
                            </div>

                            <div class="form-group col-lg-2">
                                <label>Net Wt.</label>
                                <input type="text" name="tot_net_weight" id="tot_net_weight" class="form-control" readonly tabindex="-1">
                            </div>

                            <div class="form-group col-lg-2">
                                <label>Fine Gold</label>
                                <input type="text" name="tot_fine_gold" id="tot_fine_gold" class="form-control" readonly tabindex="-1">
                            </div>

                            <div class="form-group col-lg-2">
                                <label>Amount</label>
                                <input type="text" name="tot_amount" id="tot_amount" class="form-control" readonly tabindex="-1">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Comment</label>
                            <input type="text" name="comment" id="comment" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Picture</label>
                            <input type="file" id="picture" name="picture"/>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
                    </div>
            </div>
        </div>
    </div>
    </form>
</section><!-- /.content -->
</aside><!-- /.right-side -->



<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">

$("#party_id").change(function(){
	$.ajax({
		url:"ajax.php",
		type: 'post',
		dataType: 'json',
		data:'party_id='+encodeURIComponent($(this).val()),
		beforeSend: function() {
			// execute whatever you want to execute before send to server
		},
		complete: function() {
			// execute whatever you want to execute on comlete
		},
		success: function(data) {
		    $("#getfine").html(data.tot_fine_gold);
			$("#getamt").html(data.tot_amount);
		}
	});
});

$('#gross_weight, #copper').focusout(function(){
    
    if($(this).val() != 0)
        $(this).val( parseFloat($(this).val()).toFixed(3) );
    else
        $(this).val( parseFloat(0).toFixed(3) );
});

$('#touch, #waste, #amount').focusout(function(){
    
    if($(this).val() != 0)
        $(this).val( parseFloat($(this).val()).toFixed(2) );
    else
        $(this).val( parseFloat(0).toFixed(2) );
});

// Net Weight Calculation : start
$('#gross_weight').focusout(function(){
    var copper_val = 0.00;
    if($(this).val() > 0)
        copper_val = parseFloat($('#copper').val());
    
    var net_weight_val = parseFloat($(this).val()) - parseFloat(copper_val);
    
    if(!isNaN(net_weight_val))
        $('#net_weight').val(net_weight_val.toFixed(3));
    else
        $('#net_weight').val('0.000');
});

$('#copper').focusout(function(){
    var copper_val = 0.00;
    if($(this).val() > 0)
        copper_val = parseFloat($(this).val());
    
    var net_weight_val = parseFloat($('#gross_weight').val()) - parseFloat(copper_val);
    
    if(!isNaN(net_weight_val))
        $('#net_weight').val(net_weight_val.toFixed(3));
    else
        $('#net_weight').val('0.000');
});
// Net Weight Calculation : end

// Fine Gold Calculation : Start

$('#touch').focusout(function(){
    var waste_val = 0.00;
    if($(this).val() > 0)
        waste_val = parseFloat($("#waste").val());
    
    var fine_gold_val = parseFloat($('#net_weight').val()) * (parseFloat($(this).val()) + parseFloat(waste_val)) / 100;
    
    if(!isNaN(fine_gold_val))
        $('#fine_gold').val(fine_gold_val.toFixed(3));
    else
        $('#fine_gold').val('0.000');
});

$('#waste').focusout(function(){
    var waste_val = 0.00;
    if($(this).val() > 0)
        waste_val = parseFloat($(this).val());
    
    var fine_gold_val = parseFloat($('#net_weight').val()) * (parseFloat($('#touch').val()) + parseFloat(waste_val)) / 100;
    
    if(!isNaN(fine_gold_val))
        $('#fine_gold').val(fine_gold_val.toFixed(3));
    else
        $('#fine_gold').val('0.000');
});

// Fine Gold Calculation : End


$('#additem').bind('click', function(){
    var serializeData = $('#itemEntry').find("select, input").serializeArray();
    var tableData = "<tr><td><a href='javascript:;'>Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;'>Delete</a></td>";
    
    for(var i = 0; i < serializeData.length; i++){
        tableData += "<td>";
        if(i == 0){
            tableData += $('#' + serializeData[i].name + ' option:selected').text();
        }
        else{
            tableData += serializeData[i].value;
        }
        
        tableData += "<input type='hidden' name='purchasedetail[" + serializeData[i].name + "][]' value='" + serializeData[i].value + "' />";
        tableData += "</td>";
    }
    
    tableData += "</tr>";
    
    $('#purchase_list tbody').append(tableData);
    
    var tot_gross_weight = 0;
    var tot_copper = 0;
    var tot_net_weight = 0;
    var tot_fine_gold = 0;
    var tot_amount = 0;
    $('#purchase_list tbody').find('input[name *= "gross_weight"]').each(function(){
        tot_gross_weight += parseFloat($(this).val());
    });
    $('#tot_gross_weight').val(parseFloat(tot_gross_weight).toFixed(3));
    
    $('#purchase_list tbody').find('input[name *= "copper"]').each(function(){
        tot_copper += parseFloat($(this).val());
    });
    $('#tot_copper').val(parseFloat(tot_copper).toFixed(3));
    
    $('#purchase_list tbody').find('input[name *= "net_weight"]').each(function(){
        tot_net_weight += parseFloat($(this).val());
    });
    $('#tot_net_weight').val(parseFloat(tot_net_weight).toFixed(3));
    
    $('#purchase_list tbody').find('input[name *= "fine_gold"]').each(function(){
        tot_fine_gold += parseFloat($(this).val());
    });
    $('#tot_fine_gold').val(parseFloat(tot_fine_gold).toFixed(3));
    
    $('#purchase_list tbody').find('input[name *= "amount"]').each(function(){
        tot_amount += parseFloat($(this).val());
    });
    $('#tot_amount').val(parseFloat(tot_amount).toFixed(2));
    
    $('#itemEntry').children().find('input[type="text"],select').each(function(){
        $(this).val('');
    });
    $('#item_id').focus();
});
			
</script>
    </body>
</html>