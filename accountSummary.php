<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');


if(isset($_POST['show']))
{
    
	$toDate = $_POST['toYear'] . '-' . $_POST['toMonth'] . '-' . $_POST['toDate'];
    $sSQL = "select * from account WHERE updated_at <= '$toDate'";
	
    $rs = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
}
if(isset($_GET['mode']) && isset($_GET['id']))
{
	if($_GET['mode']==1)
	{
		/*$sSQL = "SELECT * FROM account where transaction_id=".$_GET['id'];
		//echo $sSQL;die;
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0)
		{
			$row1 = mysqli_fetch_array($rs1);
		}*/
	}
	else
	{
		// summarise balance
		
		$acat = 0;
		$acft = 0;
		$adat = 0;
		$adft = 0;
		$amt = 0;
		$amt_crdr;
		$fine = 0;
		$fine_crdr;
		$ata;
		$atf;
		
		$sSQL1 = "select amount,amount_crdr,fine,fine_crdr from transaction_master where account_id1 = ".$_GET['id'];
		$rs11 = mysqli_query($dml->conn, $sSQL1) or print(mysqli_error($dml->conn));
		while($row11 = mysqli_fetch_array($rs11))
		{
			if($row11['amount_crdr'] == "CR") { $acat = $acat + $row11['amount']; }
			if($row11['fine_crdr'] == "CR") { $acft = $acft + $row11['fine']; }
			if($row11['amount_crdr'] == "DR") { $adat = $adat + $row11['amount']; }
			if($row11['fine_crdr'] == "CR") { $adft = $adft + $row11['fine']; }												
			$ata = $acat - $adat;
			$atf = $acft - $adft;
		}
		if($ata > 0)
		{
			$amt = $ata;
			$amt_crdr = "CR";
		}
		else if($ata < 0)
		{
			$amt = abs($ata);
			$amt_crdr = "DR";
		}
		else
		{
			$amt = 0;
			$amt_crdr = "CR";
		}
		if($atf > 0)
		{
			$fine = $atf;
			$fine_crdr = "CR";
		}
		else if($ata < 0)
		{
			$fine = abs($atf);
			$fine_crdr = "DR";
		}
		else
		{
			$fine = 0;
			$fine_crdr = "CR";
		}
		// delete transactions
		
		$aid1 = $_GET['id'];
		$aid2 = REMOVEABLE_ID;
		if($aid1 == $aid2)
		{
			$sSQL = "DELETE FROM transaction_master WHERE account_id1 =".$_GET['id'];
		}
		else
		{
			$sSQL = "update transaction_master set account_id1 = '$aid2' where account_id1 = '$aid1'";
		}
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="All Records are deleted successfully.";
		$timestamp = date('Y-m-d H:i:s');
		$timestamp1 = date('Y-m-d');
		
		// update summarise balance in account table
		
		$sSQL = "update account set current_amount = '$amt',current_amountcrdr = '$amt_crdr',current_fine='$fine', current_finecrdr = '$fine_crdr',updated_at = '$timestamp' WHERE account_id =".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$aid2 = OPENING_ID;
		$sSQL = "insert into transaction_master (account_id1,account_id2,current_date1,amount,amount_crdr,fine,fine_crdr,updated_at) values ('$aid1','$aid2','$timestamp1','$amt','$amt_crdr','$fine','$fine_crdr','$timestamp')";
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		header("Location:accountSummary.php");
		exit;
	}
}
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Account Summary</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="" method="post">
        <div class="row">
            <!-- left column -->
            <?php include_once('msg.php');?>
            <div class="col-md-10">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>To Date</label>
									<div class="input-group">
										<select name="toDate" id="currentDate" class="form-group pull-left">
											<?php for($i=1;$i<=31;$i++){?>
												<?php if($i < 10){ $i = '0'.$i;} ?>
												<?php if(date('d') == $i) { ?>
													<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else { ?>
													<option value="<?php echo $i;?>"><?php echo $i;?></option>
												<?php } ?>
											<?php }?>
										</select>
										<select name="toMonth" id="currentMonth" class="form-group pull-left">
										  <?php for($i=1;$i<=12;$i++){?>
											  <?php if($i < 10){ $i = '0'.$i;}?>
											  <?php if(date('m') == $i) { ?>
												  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											  <?php } else { ?>
												  <option value="<?php echo $i;?>"><?php echo $i;?></option>
											  <?php } ?>
										  <?php }?>
										</select>
										<select name="toYear" id="currentYear" class="form-group pull-left">
										  <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
											  <?php if(date('Y') == $i) { ?>
												  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											  <?php } else { ?>
												  <option value="<?php echo $i;?>"><?php echo $i;?></option>
											  <?php } ?>
										  <?php }?>
										</select>
									</div>
								</div>
										
								<div class="col-md-2">
									<label>&nbsp;</label>
									<div class="form-group">
										<input type="submit" name="show" value="Go!!" class="btn btn-primary btn-flat"/>
									</div>
								</div>
							</div>
						</div>
						<!-- Table Display-->
						<table id="partyLedgerList" class="table table-bordered">
							<thead class="multiple_header">
								<tr>
									<th>Action</th>
									<th>Serial No</th>
									<th>Account</th>
									<th colspan="2" style="text-align:center">Credit</th>
									<th colspan="2" style="text-align:center">Debit</th>
								</tr>
								<tr>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">Amount</th>
									<th style="text-align:center">Fine</th>
									<th style="text-align:center">Amount</th>
									<th style="text-align:center">Fine</th>
								</tr>
							</thead>
							<?php 
								$tac = 0;
								$tfc = 0;
								$tad = 0;
								$tfd = 0;
							?>
							<tbody>
								<?php if(isset($rs) && mysqli_num_rows($rs) > 0) { 
								$sn = 1;
								
								while($row = mysqli_fetch_array($rs)) { ?>
									<tr>
										<!--<td align="center"><a href="accountSummary.php?id=<?php //echo $row['account_id'];?>&mode=2">Remove Transactions</a></td>-->
										<td align="center"><a href="javascript:delete_summary(<?php echo $row['account_id']; ?>)">Remove Transactions</a></td>
										<td align="right"><?php echo $sn; ?></td>
										<td align="right"><?php echo $row['first_name']; ?></td>
										<?php 
											$acat = 0;
											$acft = 0;
											$adat = 0;
											$adft = 0;
											$ata;
											$atf;
											$sSQL1 = "select amount,amount_crdr,fine,fine_crdr from transaction_master where account_id1 = ".$row['account_id'];
											$rs11 = mysqli_query($dml->conn, $sSQL1) or print(mysqli_error($dml->conn));
											while($row11 = mysqli_fetch_array($rs11))
											{
												if($row11['amount_crdr'] == "CR") { $acat = $acat + $row11['amount']; }
												if($row11['fine_crdr'] == "CR") { $acft = $acft + $row11['fine']; }
												if($row11['amount_crdr'] == "DR") { $adat = $adat + $row11['amount']; }
												if($row11['fine_crdr'] == "CR") { $adft = $adft + $row11['fine']; }												
												$ata = $acat - $adat;
												$atf = $acft - $adft;
											}
										?>
										<td align="right"><?php if($ata > 0) { echo $ata; $tac = $tac + $ata; } else { echo 0; } ?></td>
										<td align="right"><?php if($atf > 0) { echo $atf; $tfc = $tfc + $atf;} else { echo 0; } ?></td>
										<td align="right"><?php if($ata < 0) { echo abs($ata); $tad = $tad + abs($ata); } else { echo 0; } ?></td>
										<td align="right"><?php if($atf < 0) { echo abs($atf); $tfd = $tfd + abs($atf); } else { echo 0; } ?></td>
									</tr>
									<?php $sn++; 
											$acat = 0;
											$acft = 0;
											$adat = 0;
											$adft = 0;
											$ata = 0;
											$atf = 0;
											} ?>
								
								<?php } else { ?>
								
									<tr>
										<td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
									</tr>
								
								<?php } ?>
							</tbody>
							<tfoot>
								<tr class="oddRow">
									<th style="text-align: right;">Summary</th>
									<th style="text-align: right;"><?php echo ""; ?></th>
									<th style="text-align: right;"><?php echo ""; ?></th>
									<th style="text-align: right;"><?php echo $tac; ?></th>
									<th style="text-align: right;"><?php echo $tfc; ?></th>
									<th style="text-align: right;"><?php echo $tad; ?></th>
									<th style="text-align: right;"><?php echo $tfd; ?></th>
								</tr>
								<tr>
									<th colspan="8">
										<div id="currentBalanceContainer" class="label-warning">
											<div id="fineGoldContent">
												<label>Current Fine:</label>
												<span id="getfine">
													<?php
														$tf = $tfc - $tfd;
														if ($tf > 0)
														{
															echo $tf."    Cr";
														}	
														else if ($tf < 0)
														{
															echo abs($tf)."    Dr";
														}
														else
														{
															echo $tf;
														}
													?>
												</span>
											</div>
											<div id="amountContent">
												<label>Current Amount:</label>
												<span id="getamt">
													<?php
														$ta = $tac - $tad;
														if ($ta > 0)
														{
															echo $ta."    Cr";
														}	
														else if ($ta < 0)
														{
															echo abs($ta)."    Dr";
														}
														else
														{
															echo $ta;	
														}
													?>
												</span>
											</div>
										</div>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
            </div>
		</div>
        </form>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl.'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

var dataArray = <?php echo $dataArray; ?>;
$('#party_id').autocomplete({
    source: dataArray,
    autoFocus: true,
    select: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    focus: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    minChars: 1
}).autocomplete()._renderItem = function( ul, item ) {
    return $( "<li>" )
    .append( "<a>" + item.label + "</a>" )
    .appendTo( ul );
};;

</script>

</body>
</html>