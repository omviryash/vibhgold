<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;

$party_name = "";
$party_id = "";

$fieldForParty=array('party_id','name');
$whereForParty='';
$orderbyForParty='party_id';
$orderForParty='ASC';
$getParty=$dml->selectWithNestedKey('party',$fieldForParty,$whereForParty,$orderbyForParty,$orderForParty);

$dataArray = "[";
for($i = 0; $i < count($getParty); $i++){
    $dataArray .= '{label : "' . $getParty[$i]['name'] . '", value : "' . $getParty[$i]['party_id'] . '"},';
}
$dataArray .= "]";

$frDate = (isset($_POST['fromDate'])) ? $_POST['fromDate'] : date('d');
$frMonth = (isset($_POST['fromMonth'])) ? $_POST['fromMonth'] : date('m');
$frYear = (isset($_POST['fromYear'])) ? $_POST['fromYear'] : date('Y');

$toDt = (isset($_POST['toDate'])) ? $_POST['toDate'] : date('d');
$toMnth = (isset($_POST['toMonth'])) ? $_POST['toMonth'] : date('m');
$toYr = (isset($_POST['toYear'])) ? $_POST['toYear'] : date('Y');

$fromDate = date('Y-m-d');
$toDate = date('Y-m-d');

$tot_credit_fine = $tot_credit_amount = $tot_debit_fine = $tot_debit_amount = 0;

if(isset($_POST['fromDate']) && $_POST['hidden_party_id'] != ""){
    
    $party_name = $_POST['party_id'];
    $party_id = $_POST['hidden_party_id'];
    
    $fromDate = $_POST['fromYear'] . '-' . $_POST['fromMonth'] . '-' . $_POST['fromDate'];
    $toDate = $_POST['toYear'] . '-' . $_POST['toMonth'] . '-' . $_POST['toDate'];
    $listData = array();
    
    $purchaseQuery = "SELECT purchase_date, tot_amount, tot_fine_gold
                FROM purchase
                WHERE party_id = " . $_POST['hidden_party_id'] . "
                AND purchase_date >= '" . $fromDate . "' AND purchase_date <= '" . $toDate . "'";
    $purchaseQueryResult = mysqli_query($dml->conn, $purchaseQuery);
    
    if(mysqli_num_rows($purchaseQueryResult)){
        while($purchase_data = mysqli_fetch_assoc($purchaseQueryResult)){
            $listData['dates'][] = $purchase_data['purchase_date'];
            $listData['purchase_fine'][] = $purchase_data['tot_fine_gold'];
            $listData['purchase_amount'][] = $purchase_data['tot_amount'];
            $listData['payment_fine'][] = 0;
            $listData['payment_amount'][] = 0;
            
            $tot_credit_fine = $tot_credit_fine + number_format($purchase_data['tot_fine_gold'], 3, '.' , '');
            $tot_credit_amount = $tot_credit_amount + number_format($purchase_data['tot_amount'], 3, '.' , '');
            
            $tot_debit_fine = $tot_debit_fine + number_format(0, 3, '.' , '');
            $tot_debit_amount = $tot_debit_amount + number_format(0, 3, '.' , '');
        }
    }
    
    $paymentQuery = "SELECT payment_date, fine_gold, amount
                FROM payment
                WHERE party_id = " . $_POST['hidden_party_id'] . "
                AND payment_date >= '" . $fromDate . "' AND payment_date <= '" . $toDate . "'";
    $paymentQueryResult = mysqli_query($dml->conn, $paymentQuery);
    
    if(mysqli_num_rows($paymentQueryResult)){
        while($payment_data = mysqli_fetch_assoc($paymentQueryResult)){
            $listData['dates'][] = $payment_data['payment_date'];
            $listData['purchase_fine'][] = 0;
            $listData['purchase_amount'][] = 0;
            $listData['payment_fine'][] = $payment_data['fine_gold'];
            $listData['payment_amount'][] = $payment_data['amount'];
            
            $tot_credit_fine = $tot_credit_fine + number_format(0, 3, '.' , '');
            $tot_credit_amount = $tot_credit_amount + number_format(0, 3, '.' , '');
            
            $tot_debit_fine = $tot_debit_fine + number_format($payment_data['fine_gold'], 3, '.' , '');
            $tot_debit_amount = $tot_debit_amount + number_format($payment_data['amount'], 3, '.' , '');
        }
    }
    if($listData['purchase_fine'] && $listData['purchase_amount'] && $listData['payment_fine'] && $listData['payment_amount']) {
    array_multisort(
        $listData['dates'], SORT_ASC,
        $listData['purchase_fine'], SORT_REGULAR,
        $listData['purchase_amount'], SORT_REGULAR,
        $listData['payment_fine'], SORT_REGULAR,
        $listData['payment_amount'], SORT_REGULAR
    );
	} else {
		array_multisort(
			$listData['dates'], SORT_ASC
		);
	}
}


include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
		
?>
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Party Ledger</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="" method="post">
        <div class="row">
            <!-- left column -->
            <?php include_once('msg.php');?>
            <div class="col-md-10">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label>Party</label>
                                <input type="text" name="party_id" id="party_id" class="form-control" value="<?php echo $party_name; ?>" />
                                <input type="hidden" name="hidden_party_id" id="hidden_party_id" value="<?php echo $party_id; ?>" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>From Date</label> 
                                        <div class="input-group">
                                            <select name="fromDate" id="currentDate" class="form-group pull-left">
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <?php if($i < 10){ $i = '0'.$i;} ?>
                                                    <?php if($frDate == $i) { ?>
                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            <select name="fromMonth" id="currentMonth" class="form-group pull-left">
                                              <?php for($i=1;$i<=12;$i++){?>
                                                  <?php if($i < 10){ $i = '0'.$i;}?>
                                                  <?php if($frMonth == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                            <select name="fromYear" id="currentYear" class="form-group pull-left">
                                              <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                  <?php if($frYear == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label>To Date</label>
                                        <div class="input-group">
                                            <select name="toDate" id="currentDate" class="form-group pull-left">
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <?php if($i < 10){ $i = '0'.$i;} ?>
                                                    <?php if($toDt == $i) { ?>
                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            <select name="toMonth" id="currentMonth" class="form-group pull-left">
                                              <?php for($i=1;$i<=12;$i++){?>
                                                  <?php if($i < 10){ $i = '0'.$i;}?>
                                                  <?php if($toMnth == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                            <select name="toYear" id="currentYear" class="form-group pull-left">
                                              <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                  <?php if($toYr == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <label>&nbsp;</label>
                                        <div class="form-group">
                                            <input type="submit" name="show" value="Show" class="btn btn-primary btn-flat"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
									
                            <!-- Table Display-->
                        <table id="partyLedgerList" class="table table-bordered">
                            <thead class="multiple_header">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th colspan="2" style="text-align:center">Credit</th>
                                    <th colspan="2" style="text-align:center">Debit</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Date</th>
                                    <th style="text-align:center">Fine</th>
                                    <th style="text-align:center">Amount</th>
                                    <th style="text-align:center">Fine</th>
                                    <th style="text-align:center">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($listData) && count($listData) > 0) { ?>
                                    <?php for($i = 0; $i < count($listData['dates']); $i++) { ?>
                                    <tr>
                                        <td align="center"><?php echo $fun->date_ymd_to_dmy($listData['dates'][$i]); ?></td>
                                        <td align="right"><?php echo number_format($listData['purchase_fine'][$i], 3, '.' , ''); ?></td>
                                        <td align="right"><?php echo number_format($listData['purchase_amount'][$i], 2, '.' , ''); ?></td>
                                        <td align="right"><?php echo number_format($listData['payment_fine'][$i], 3, '.' , ''); ?></td>
                                        <td align="right"><?php echo number_format($listData['payment_amount'][$i], 2, '.' , ''); ?></td>
                                    </tr>
                                    <?php } ?>
                                
                                <?php } else { ?>
                                
                                    <tr>
                                        <td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
                                    </tr>
                                
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="oddRow">
                                    <th style="text-align: right;">Total</th>
                                    <th style="text-align: right;"><?php echo number_format($tot_credit_fine, 3, '.' , ''); ?></th>
                                    <th style="text-align: right;"><?php echo number_format($tot_credit_amount, 2, '.' , ''); ?></th>
                                    <th style="text-align: right;"><?php echo number_format($tot_debit_fine, 3, '.' , ''); ?></th>
                                    <th style="text-align: right;"><?php echo number_format($tot_debit_amount, 2, '.' , ''); ?></th>
                                </tr>
                                
                                <tr>
                                    <th colspan="5">
                                        <div id="currentBalanceContainer" class="label-warning">
                                            <div id="fineGoldContent">
                                            <label>Current Fine:</label>
                                            <span id="getfine">
                                                <?php
                                                    $calc_current_fine = number_format($tot_credit_fine, 3, '.' , '') - number_format($tot_debit_fine, 3, '.' , '');
                                                    echo number_format($calc_current_fine, 3, '.' , '')
                                                ?>
                                            </span>
                                        </div>
                                        <div id="amountContent">
                                            <label>Current Amount:</label>
                                            <span id="getamt">
                                                <?php
                                                    $calc_current_amount = number_format($tot_credit_amount, 2, '.' , '') - number_format($tot_debit_amount, 2, '.' , '');
                                                    echo number_format($calc_current_amount, 2, '.' , '')
                                                ?>
                                            </span>
                                        </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl.'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

var dataArray = <?php echo $dataArray; ?>;
$('#party_id').autocomplete({
    source: dataArray,
    autoFocus: true,
    select: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    focus: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    minChars: 1
}).autocomplete()._renderItem = function( ul, item ) {
    return $( "<li>" )
    .append( "<a>" + item.label + "</a>" )
    .appendTo( ul );
};;

</script>

</body>
</html>