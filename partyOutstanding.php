<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

$fieldForParty=array('party_id', 'name', 'current_fine_gold', 'current_amount', 'created_at','current_finecrdr','current_amountcrdr');
$whereForParty='';
$orderbyForParty='party_id';
$orderForParty='ASC';
$getParty=$dml->selectWithNestedKey('party',$fieldForParty,$whereForParty,$orderbyForParty,$orderForParty);

$tot_credit_fine = $tot_credit_amount = $tot_debit_fine = $tot_debit_amount = 0;

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

?>
<aside class="right-side strech">                
    <section class="content-header">
        <h1>Party Outstanding</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <?php include_once('msg.php');?>
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                        <table id="party_outstanding_list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">No.</th>
                                    <th rowspan="2">Party Name</th>
                                    <th colspan="2" style="text-align:center">Credit</th>
                                    <th colspan="2" style="text-align:center">Debit</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Fine</th>
                                    <th style="text-align:center">Amount</th>
                                    <th style="text-align:center">Fine</th>
                                    <th style="text-align:center">Amount</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php if(count($getParty) > 0) { $rowCount = 1; ?>

                                    <?php foreach($getParty as $key => $value) {
									
										$fieldForPurchage		= array('party_id', 'tot_fine_gold', 'tot_amount');
										$whereForPurchage		= ' party_id = '.$value['party_id'];
										$orderbyForPurchage		= '';
										$orderForPurchage		= '';
										$getPurchages			= $dml->selectWithNestedKey('purchase',$fieldForPurchage,$whereForPurchage,$orderbyForPurchage,$orderForPurchage);
										$purchage_fine_gold 	= 0;
										$purchage_amount 		= 0;

										$current_fine_gold 	= $value['current_fine_gold'];
										$current_amount 	= $value['current_amount'];
										if($value['current_amountcrdr'] == 'debit') {
											$current_amount = ($value['current_amount'] * -1);
										}
										if($value['current_finecrdr'] == 'debit') {
											$current_fine_gold = ($value['current_fine_gold'] * -1);
										}


										if($getPurchages) {
											foreach ($getPurchages as $getPurchage) {
												$purchage_fine_gold += $getPurchage['tot_fine_gold'];
												$purchage_amount    += $getPurchage['tot_amount'];
											}
										}

										$fieldForPayment		= array('party_id', 'fine_gold', 'amount');
										$whereForPayment		= ' party_id = '.$value['party_id'];
										$orderbyForPayment		= '';
										$orderForPayment		= '';
										$getPayments			= $dml->selectWithNestedKey('payment',$fieldForPayment,$whereForPayment,$orderbyForPayment,$orderForPayment);
										$payment_fine_gold 		= 0;
										$payment_amount 		= 0;

										if($getPayments) {
											foreach ($getPayments as $getPayment) {
												$payment_fine_gold += $getPayment['fine_gold'];
												$payment_amount    += $getPayment['amount'];
											}
										}

										$tot_fine_gold  = 0;
										$tot_amount	    = 0;

										$tot_fine_gold 	= $current_fine_gold + $purchage_fine_gold - $payment_fine_gold;
										$tot_amount 	= $current_amount + $purchage_amount - $payment_amount;
										?>
                                        <tr>
                                            <td align="center"><?php echo $rowCount; ?></td>
                                            <td align="center"><?php echo $value['name']; ?></td>
                                            <?php if($tot_fine_gold >= 0) { $tot_credit_fine += $tot_fine_gold; ?>
                                                <td align="right"><?php echo number_format($tot_fine_gold, 3, '.' , ''); ?></td>
                                            <?php } else { ?>
                                                <td align="right">&nbsp;</td>
                                            <?php } ?>
                                                
                                            <?php if($tot_amount >= 0) { $tot_credit_amount += $tot_amount; ?>
                                                <td align="right"><?php echo number_format($tot_amount, 3, '.' , ''); ?></td>
                                            <?php } else { ?>
                                                <td align="right">&nbsp;</td>
                                            <?php } ?>
                                                
                                            <?php if($tot_fine_gold < 0) { $tot_debit_fine += $tot_fine_gold; ?>
                                                <td align="right"><?php echo number_format($tot_fine_gold, 3, '.' , ''); ?></td>
                                            <?php } else { ?>
                                                <td align="right">&nbsp;</td>
                                            <?php } ?>
                                            
                                            <?php if($tot_amount < 0) { $tot_debit_amount += $tot_amount; ?>
                                                <td align="right"><?php echo number_format($tot_amount, 3, '.' , ''); ?></td>
                                            <?php } else { ?>
                                                <td align="right">&nbsp;</td>
                                            <?php } ?>
                                        </tr>
                                    <?php $rowCount++; } ?>
                                <?php } else { ?>
                                <tr>
                                    <td colspan="7"><span class="alert-danger">No records found.</span></td>
                                </tr>
                                
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="oddRow">
                                    <th colspan="2" style="text-align:right">Total</th>
                                    <th style="text-align:right"><?php echo number_format($tot_credit_fine, 3, '.' , ''); ?></th>
                                    <th style="text-align:right"><?php echo number_format($tot_credit_amount, 2, '.' , ''); ?></th>
                                    <th style="text-align:right"><?php echo number_format($tot_debit_fine, 3, '.' , ''); ?></th>
                                    <th style="text-align:right"><?php echo number_format($tot_debit_amount, 2, '.' , ''); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
</body>
</html>