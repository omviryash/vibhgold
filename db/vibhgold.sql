-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 25, 2015 at 03:37 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `deep`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `current_amount` double NOT NULL,
  `current_amountcrdr` enum('DR','CR') NOT NULL,
  `current_fine` double NOT NULL,
  `current_finecrdr` enum('DR','CR') NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(30) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `state` varchar(20) NOT NULL,
  `account_status` enum('A','I') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `first_name`, `last_name`, `current_amount`, `current_amountcrdr`, `current_fine`, `current_finecrdr`, `address`, `city`, `phone1`, `state`, `account_status`, `created_at`, `updated_at`, `comment`) VALUES
(18, 'Cash', '', 0, 'DR', 0, 'DR', '', ' ', '', 'GJ', 'A', '2015-07-23 22:09:52', '2015-07-23 22:09:52', ''),
(19, 'Bank1', '', 0, 'DR', 0, 'DR', '', ' ', '', 'GJ', 'A', '2015-07-23 22:10:15', '2015-07-23 22:10:15', ''),
(20, 'Opening', '', 0, 'DR', 0, 'DR', '', ' ', '', 'GJ', 'A', '2015-07-23 22:10:37', '2015-07-23 22:10:37', ''),
(21, 'My Gold', '', 0, 'DR', 0, 'DR', '', ' ', '', 'GJ', 'A', '2015-07-23 22:11:01', '2015-07-23 22:11:01', ''),
(22, 'For Removal Transaction', '', 0, 'DR', 0, 'DR', '', ' ', '', 'GJ', 'A', '2015-07-23 22:12:36', '2015-07-23 22:12:36', ''),
(27, 'Chirag', 'Kotak', 2000, 'DR', 210, 'DR', 'Main Bazar', 'Talala', '9876543210', 'GJ', 'A', '2015-07-25 15:36:56', '2015-07-25 15:36:56', 'Testing by Mahendra'),
(28, 'Mahendra', 'Vala', 3500, 'DR', 100, 'DR', '34 Vadi Vistar', 'Maljinjava', '9876543210', 'GJ', 'A', '2015-07-25 15:43:10', '2015-07-25 15:43:10', 'Testing by Mahendra');

-- --------------------------------------------------------

--
-- Table structure for table `future`
--

CREATE TABLE IF NOT EXISTS `future` (
  `future_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `future_date` date NOT NULL,
  `future_fine` int(11) NOT NULL,
  `future_buy_sell` enum('Buy','Sell') NOT NULL,
  `parity` int(11) NOT NULL,
  `future_amount` int(11) NOT NULL,
  `future_amountcrdr` enum('DR','CR') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`future_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `future`
--

INSERT INTO `future` (`future_id`, `account_id`, `future_date`, `future_fine`, `future_buy_sell`, `parity`, `future_amount`, `future_amountcrdr`, `created_at`, `updated_at`) VALUES
(11, 18, '2015-07-23', 1000, 'Buy', 100, 100, 'DR', '2015-07-23 19:01:13', '2015-07-23 19:01:13'),
(12, 27, '2015-07-25', 100, 'Buy', 10, 13900, 'DR', '2015-07-25 12:40:09', '2015-07-25 12:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) DEFAULT NULL,
  `item_image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `item_name`, `item_image`, `created_at`, `updated_at`) VALUES
(3, 'Chain', '', '2014-08-16 18:00:26', '2014-08-16 18:00:26'),
(4, 'HG Emerald', '', '2014-08-16 18:00:45', '2014-08-16 18:00:45'),
(5, 'HG RJT Casting', '', '2014-08-16 18:01:10', '2014-08-16 18:01:10'),
(6, 'Om Pendal', '', '2015-05-23 11:04:00', '2015-05-23 11:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `party`
--

CREATE TABLE IF NOT EXISTS `party` (
  `party_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` text,
  `city_village` varchar(50) DEFAULT NULL,
  `phone1` varchar(30) DEFAULT NULL,
  `phone2` varchar(30) DEFAULT NULL,
  `current_fine_gold` double DEFAULT NULL,
  `current_finecrdr` varchar(15) DEFAULT NULL,
  `current_amount` double DEFAULT NULL,
  `current_amountcrdr` varchar(15) DEFAULT NULL,
  `open_fine_gold` double DEFAULT NULL,
  `open_finecrd` varchar(15) DEFAULT NULL,
  `open_amount` double DEFAULT NULL,
  `open_amountcrdr` varchar(15) DEFAULT NULL,
  `comments` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`party_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `party`
--

INSERT INTO `party` (`party_id`, `name`, `address`, `city_village`, `phone1`, `phone2`, `current_fine_gold`, `current_finecrdr`, `current_amount`, `current_amountcrdr`, `open_fine_gold`, `open_finecrd`, `open_amount`, `open_amountcrdr`, `comments`, `created_at`, `updated_at`) VALUES
(2, 'Test1', 'Krishna', 'Rajkot', '919727691355', '919727691355', 1, 'debit', 2, 'debit', 1, 'debit', 2, 'debit', '', '2014-08-09 14:05:06', '2014-08-09 14:05:06'),
(3, 'HG', '', 'Rajkot', '', '', 100, 'debit', 1000, 'debit', 100, 'debit', 1000, 'debit', '', '2014-08-16 17:59:52', '2014-08-16 17:59:52'),
(4, 'Vipul', 'Krishna, 3/11 Gayatrinagar', 'Rajkot', '9727691355', '9898855455', 100, 'debit', 1000, 'credit', 100, 'debit', 1000, 'debit', 'ccccccccccccooooooooommmmmmmmme', '2015-05-09 19:24:41', '2015-05-09 19:34:57'),
(5, 'May23_1', '', 'Rajkot', '', '', 100, 'debit', 1000, 'credit', 100, 'debit', 1000, 'debit', 'Om', '2015-05-23 10:58:57', '2015-05-23 10:59:28'),
(6, 'June_06_01', 'Om', 'Rajkot', '12', '23', 100, 'debit', 1000, 'credit', 100, 'debit', 1000, 'credit', 'Debit Credit', '2015-06-06 15:40:12', '2015-06-06 15:40:12'),
(7, 'June_06_02', 'Om2', 'Rajkot', '111', '222', 100, 'debit', 1000, 'credit', 100, 'debit', 1000, 'credit', 'OM', '2015-06-06 15:52:16', '2015-06-06 15:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) DEFAULT NULL,
  `fine_gold` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `comments` text,
  `payment_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `party_id`, `fine_gold`, `amount`, `comments`, `payment_date`, `created_at`, `updated_at`) VALUES
(1, 4, 22, 222, 'c3', '2015-05-09', '2015-05-09 16:20:01', '2015-05-09 16:20:01'),
(2, 5, 9.48, 2000, 'Om', '2015-05-23', '2015-05-23 07:33:41', '2015-05-23 07:33:41'),
(3, 5, 400, 9000, 'Om', '2015-06-06', '2015-06-06 03:37:52', '2015-06-06 03:37:52'),
(4, 5, 0, 2500, '', '2015-06-06', '2015-06-06 03:38:17', '2015-06-06 03:38:17'),
(5, 7, 452, 0, '', '2015-06-06', '2015-06-06 10:28:17', '2015-06-06 10:28:17'),
(6, 7, 0, 30000, '', '2015-06-06', '2015-06-06 10:28:46', '2015-06-06 10:28:46');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `party_id` int(11) DEFAULT NULL,
  `tot_gross_weight` double DEFAULT NULL,
  `tot_copper` double DEFAULT NULL,
  `tot_net_weight` double DEFAULT NULL,
  `tot_fine_gold` double DEFAULT NULL,
  `tot_amount` double DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `comments` varchar(1000) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `party_id`, `tot_gross_weight`, `tot_copper`, `tot_net_weight`, `tot_fine_gold`, `tot_amount`, `purchase_date`, `comments`, `picture`, `created_at`, `updated_at`) VALUES
(1, 4, 1111, 111, 1000, 902, 22600, '2015-05-09', 'commmmment', 'upload/purchase/', '2015-05-09 16:14:42', '2015-05-09 16:14:42'),
(2, 5, 300, 3, 297, 199.98, 10000, '2015-05-23', '', 'upload/purchase/', '2015-05-23 07:31:23', '2015-05-23 07:31:23'),
(3, 5, 78, 1, 77, 0, 900, '2015-05-23', '', 'upload/purchase/', '2015-05-23 07:32:11', '2015-05-23 07:32:11'),
(4, 5, 178, 1, 177, 49.5, 100, '2015-05-23', '', 'upload/purchase/', '2015-05-23 07:33:10', '2015-05-23 07:33:10'),
(5, 5, 100, 10, 90, 45.9, 1000, '2015-06-06', '', 'upload/purchase/', '2015-06-06 03:37:02', '2015-06-06 03:37:02'),
(6, 7, 3300, 300, 3000, 2000, 15000, '2015-06-06', '', 'upload/purchase/', '2015-06-06 10:24:34', '2015-06-06 10:24:34'),
(7, 7, 110, 10, 100, 52, 4000, '2015-06-06', '', 'upload/purchase/', '2015-06-06 10:27:19', '2015-06-06 10:27:19'),
(8, 0, 0, 0, 0, 0, 0, '2015-06-11', '', 'upload/purchase/', '2015-06-11 18:59:14', '2015-06-11 18:59:14'),
(9, 0, 0, 0, 0, 0, 0, '2015-06-11', '', 'upload/purchase/', '2015-06-11 18:59:43', '2015-06-11 18:59:43'),
(10, 3, 7.22, 0, 7.22, 7.148, 0, '2015-06-11', '', 'upload/purchase/', '2015-06-11 19:01:47', '2015-06-11 19:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `purchasedetail`
--

CREATE TABLE IF NOT EXISTS `purchasedetail` (
  `purchase_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `gross_weight` double DEFAULT NULL,
  `copper` double DEFAULT NULL,
  `net_weight` double DEFAULT NULL,
  `touch` double DEFAULT NULL,
  `waste` double DEFAULT NULL,
  `fine_gold` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`purchase_detail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `purchasedetail`
--

INSERT INTO `purchasedetail` (`purchase_detail_id`, `purchase_id`, `item_id`, `gross_weight`, `copper`, `net_weight`, `touch`, `waste`, `fine_gold`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1000, 100, 900, 88, 2, 810, 22600, '2015-05-09 16:14:42', '2015-05-09 16:14:42'),
(2, 1, 4, 111, 11, 100, 90, 2, 92, 0, '2015-05-09 16:14:42', '2015-05-09 16:14:42'),
(3, 1, 3, 100, 1, 99, 99, 1, 99, 10000, '2015-05-23 07:31:23', '2015-05-23 07:31:23'),
(4, 1, 4, 200, 2, 198, 50, 1, 100.98, 0, '2015-05-23 07:31:23', '2015-05-23 07:31:23'),
(5, 1, 3, 78, 1, 77, 0, 0, 0, 900, '2015-05-23 07:32:11', '2015-05-23 07:32:11'),
(6, 1, 4, 78, 0, 78, 0, 0, 0, 100, '2015-05-23 07:33:10', '2015-05-23 07:33:10'),
(7, 1, 3, 100, 1, 99, 50, 0, 49.5, 0, '2015-05-23 07:33:10', '2015-05-23 07:33:10'),
(8, 1, 3, 100, 10, 90, 50, 1, 45.9, 1000, '2015-06-06 03:37:02', '2015-06-06 03:37:02'),
(9, 1, 5, 1100, 100, 1000, 92, 8, 1000, 10000, '2015-06-06 10:24:34', '2015-06-06 10:24:34'),
(10, 1, 3, 2200, 200, 2000, 45, 5, 1000, 5000, '2015-06-06 10:24:34', '2015-06-06 10:24:34'),
(11, 1, 4, 110, 10, 100, 50, 2, 52, 4000, '2015-06-06 10:27:19', '2015-06-06 10:27:19'),
(12, 1, 0, 7.22, 0, 7.22, 92, 7, 7.148, 0, '2015-06-11 19:01:47', '2015-06-11 19:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_master`
--

CREATE TABLE IF NOT EXISTS `transaction_master` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id1` int(11) NOT NULL,
  `account_id2` int(11) NOT NULL,
  `current_date1` date NOT NULL,
  `amount` double NOT NULL,
  `amount_crdr` enum('CR','DR') NOT NULL,
  `gold` int(11) NOT NULL,
  `touch` int(11) NOT NULL,
  `fine` double NOT NULL,
  `fine_crdr` enum('CR','DR') NOT NULL,
  `gold_rate` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `transaction_master`
--

INSERT INTO `transaction_master` (`transaction_id`, `account_id1`, `account_id2`, `current_date1`, `amount`, `amount_crdr`, `gold`, `touch`, `fine`, `fine_crdr`, `gold_rate`, `note`, `created_at`, `updated_at`) VALUES
(4, 18, 20, '2015-07-23', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-07-23 22:09:52', '2015-07-23 22:09:52'),
(5, 19, 20, '2015-07-23', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-07-23 22:10:15', '2015-07-23 22:10:15'),
(6, 20, 20, '2015-07-23', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-07-23 22:10:37', '2015-07-23 22:10:37'),
(7, 21, 20, '2015-07-23', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-07-23 22:11:01', '2015-07-23 22:11:01'),
(8, 22, 20, '2015-07-23', 0, 'DR', 0, 0, 0, 'DR', 0, '', '2015-07-23 22:12:36', '2015-07-23 22:12:36'),
(10, 18, 23, '2015-07-23', 1000, 'DR', 150, 100, 150, 'CR', 100, 'Cash Sell', '2015-07-23 19:01:12', '2015-07-23 19:01:12'),
(12, 23, 22, '2015-07-23', 1100, 'DR', 0, 0, 0, 'DR', 0, '', '0000-00-00 00:00:00', '2015-07-23 19:18:42'),
(17, 26, 20, '2015-07-24', 100, 'DR', 0, 0, 10, 'CR', 0, '', '2015-07-24 15:54:52', '2015-07-24 15:54:52'),
(18, 25, 20, '2015-07-24', 3990, 'CR', 0, 0, 0, 'CR', 0, '', '0000-00-00 00:00:00', '2015-07-24 16:09:37'),
(19, 27, 20, '2015-07-25', 2000, 'DR', 0, 0, 210, 'CR', 0, '', '2015-07-25 15:36:56', '2015-07-25 15:36:56'),
(20, 28, 20, '2015-07-25', 3500, 'CR', 0, 0, 100, 'CR', 0, '', '2015-07-25 15:43:10', '2015-07-25 15:43:10'),
(21, 27, 18, '2015-07-25', 3500, 'DR', 150, 90, 135, 'DR', 25400, 'This is test note', '2015-07-25 12:14:24', '2015-07-25 12:14:24'),
(22, 27, 19, '2015-07-25', 2145, 'DR', 150, 80, 120, 'DR', 25400, 'This is test note', '2015-07-25 12:40:09', '2015-07-25 12:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(150) NOT NULL,
  `userType` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `fullName`, `username`, `password`, `thumbnail`, `userType`) VALUES
(1, 'Admin s', 'admin', 'admin', 'Penguins_1406570410.jpg', 1);
