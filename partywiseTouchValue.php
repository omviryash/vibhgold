<?php
	include_once('includes/header.php');
	include_once('includes/topheader.php');
	include_once('includes/leftside.php');
		
?>
<aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Partywise Touch Value
                       <!-- <small>it all starts here</small>-->
                    </h1>
                    <!--<ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>-->
                </section>

                <!-- Main content -->
                <section class="content">
                 <div class="row">
					 <!-- left column -->
						<?php include_once('msg.php');?>
                        <div class="col-md-5">
							<div class="box box-primary">
							<form action="" method="post">
								<div class="box-body">
									<div class="form-group">
										<label>Party</label>
										<select class="form-control" name="party" id="party">
											<option value="1">Vikas</option>
											<option value="2">Amit</option>
										</select>
									</div>
									<div class="form-group">									
                                        <label>Item </label>                                      
                                         <select class="form-control" name="Item" id="Item">
													<option value="1">Gold</option>
													<option value="2">Silver</option>
													<option value="3">USDINR</option>
										</select>                 
							        </div>
									<div class="form-group">
                                            <label>Touch</label>
                                            <input type="text" name="touch" id="touch" class="form-control">
                                    </div>
									
									<div class="form-group">
                                            <label>Waste</label>
                                            <input type="text" name="waste" id="waste" class="form-control">
                                    </div>
								</div>
								<div class="box-footer">
                                        <input type="submit" name="add" value="Add" class="btn btn-primary"/>
                                 </div>
							</form>
							</div>
						</div>
						<div class="col-md-5">
									<div class="box box-primary">
										<div class="box-header">
											<h3 class="box-title">Current Party's Item</h3>
										</div>
										<div class="box-body">
											<table class="table table-bordered">
												<tbody>
													<tr>														
														<th>Item</th>
														<th>Touch(%)</th>
														<th>Waste(%)</th>
													</tr>
													<tr>														
														<td>Gold</td>
														<td>10</td>
														<td>20</td>
													</tr>
													<tr>														
														<td>Gold</td>
														<td>10</td>
														<td>5</td>
													</tr>
													<tr>														
														<td>Gold</td>
														<td>20</td>
														<td>20</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
				</div>
			</section><!-- /.content -->
    </aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>