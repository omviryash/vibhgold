<?php 
session_start();
include_once('includes/basepath.php');
include_once('includes/header.php');
?>
<body class="bg-black">

	<div class="form-box" id="login-box">
	  <?php include_once('msg.php');?>
	  <div class="header">Admin Panel</div>
	  <form action="dologin.php" method="post" name="login" id="login">
	  <input type="hidden" name="frmPosted" value="1">	
		<div class="body bg-gray">
		  <div class="form-group">
			<input type="text" name="username" autofocus="" id="username" class="form-control" placeholder="username" autocomplete="off"/>
			</div>
		  <div class="form-group">
			<input type="password" name="pwd" id="pwd" class="form-control" placeholder="Password" autocomplete="off"/>
		  </div>
		</div>
		<div class="footer">
		  <button type="submit" class="btn bg-olive btn-block">Login</button>
		</div>
	  </form>
	</div>
	<?php include_once('includes/jsfiles.php'); ?>
	<script src="<?php echo $baseUrl.'js/'; ?>login.js" type="text/javascript"></script>
