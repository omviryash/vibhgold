<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

if(isset($_POST['show']))
{
    $account_id = $_POST['account_id'];
   
	$fromDate = $_POST['fromYear'] . '-' . $_POST['fromMonth'] . '-' . $_POST['fromDate'];
    $toDate = $_POST['toYear'] . '-' . $_POST['toMonth'] . '-' . $_POST['toDate'];
    
    $sSQL = "select * from future WHERE account_id = '$account_id' AND future_date >= '$fromDate' AND future_date <= '$toDate'";
	
    $rs = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
}
if(isset($_GET['mode']) && isset($_GET['fid']))
{
	if($_GET['mode']==1)
	{
		$sSQL = "SELECT * FROM future where future_id=".$_GET['fid'];
		//echo $sSQL;die;
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0)
		{
			$row1 = mysqli_fetch_array($rs1);
		}
	}
	else
	{
		$sSQL = "DELETE FROM future WHERE future_id=".$_GET['fid'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:futureReport.php");exit;
	}
}

?>
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Future Report</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <form action="" method="post">
        <div class="row">
            <!-- left column -->
            <?php include_once('msg.php');?>
            <div class="col-md-10">
                <div class="box box-primary">
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="form-group col-lg-3">
                                <label>Account</label>
                                <select class="form-control" name="account_id" id="account_id">
									<option value="">Select Account</option>
									<?php  	$sSQL = "select * from account order by first_name";
											$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
											while($row=mysqli_fetch_array($rs1))
											{ ?>
												<option value="<?php echo $row['account_id']; ?>" <?php if (isset($account_id) && $account_id == $row['account_id']) { echo "selected=selected"; } ?>><?php echo $row['first_name']; ?></option>
									<?php } ?>
								</select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>From Date</label> 
                                        <div class="input-group">
                                            <select name="fromDate" id="currentDate" class="form-group pull-left">
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <?php if($i < 10){ $i = '0'.$i;} ?>
                                                    <?php if(date('d') == $i) { ?>
                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            <select name="fromMonth" id="currentMonth" class="form-group pull-left">
                                              <?php for($i=1;$i<=12;$i++){?>
                                                  <?php if($i < 10){ $i = '0'.$i;}?>
                                                  <?php if(date('m') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                            <select name="fromYear" id="currentYear" class="form-group pull-left">
                                              <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                  <?php if(date('Y') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <label>To Date</label>
                                        <div class="input-group">
                                            <select name="toDate" id="currentDate" class="form-group pull-left">
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <?php if($i < 10){ $i = '0'.$i;} ?>
                                                    <?php if(date('d') == $i) { ?>
                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                    <?php } ?>
                                                <?php }?>
                                            </select>
                                            <select name="toMonth" id="currentMonth" class="form-group pull-left">
                                              <?php for($i=1;$i<=12;$i++){?>
                                                  <?php if($i < 10){ $i = '0'.$i;}?>
                                                  <?php if(date('m') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                            <select name="toYear" id="currentYear" class="form-group pull-left">
                                              <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                                  <?php if(date('Y') == $i) { ?>
                                                      <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                  <?php } ?>
                                              <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <label>&nbsp;</label>
                                        <div class="form-group">
                                            <input type="submit" name="show" value="Go!!" class="btn btn-primary btn-flat"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
									
                            <!-- Table Display-->
                        <table id="partyLedgerList" class="table table-bordered">
                            <thead class="multiple_header">
                                <tr>
                                    <th>Action</th>
									<th>Serial No</th>
									<th>Date</th>
									<th colspan="2" style="text-align:center">Credit</th>
                                    <th colspan="2" style="text-align:center">Debit</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">&nbsp;</th>
									<th style="text-align:center">Amount</th>
                                    <th style="text-align:center">Fine</th>
                                    <th style="text-align:center">Amount</th>
									<th style="text-align:center">Fine</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
								$sn = 1;
								$cat = 0;
								$cft = 0;
								$dat = 0;
								$dft = 0;
								?>
								<?php if(isset($rs) && mysqli_num_rows($rs) > 0) { 
								
								while($row = mysqli_fetch_array($rs)) { ?>
                                    <tr>
                                        <td align="center"><a href="entryPayment.php?fid=<?php echo $row['future_id'];?>&mode=1">Edit</a> | <!--<a href="futureReport.php?fid=<?php //echo $row['future_id'];?>&mode=2">Delete</a></td>-->
                                        <a href="javascript:delete_ftran(<?php echo $row['future_id']; ?>)">Delete</a></td>
										<td align="right"><?php echo $sn; ?></td>
                                        <td align="right"><?php echo $row['future_date']; ?></td>
                                        <td align="right"><?php if($row['future_amountcrdr'] == "CR") { echo $row['future_amount']; $cat = $cat + $row['future_amount']; } ?></td>
										<td align="right"><?php if($row['future_buy_sell'] == "Sell") { echo $row['future_fine']; $cft = $cft + $row['future_fine']; } ?></td>
										<td align="right"><?php if($row['future_amountcrdr'] == "DR") { echo $row['future_amount']; $dat = $dat + $row['future_amount']; } ?></td>
										<td align="right"><?php if($row['future_buy_sell'] == "Buy") { echo $row['future_fine']; $dft = $dft + $row['future_fine']; } ?></td>
                                    </tr>
                                    <?php $sn++; } ?>
                                
                                <?php } else { ?>
                                
                                    <tr>
                                        <td colspan="5"><span class="alert-danger">No records found for selected party and given dates.</span></td>
                                    </tr>
                                
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="oddRow">
                                    <th style="text-align: right;">Summary</th>
                                    <th style="text-align: right;"><?php echo ""; ?></th>
                                    <th style="text-align: right;"><?php echo ""; ?></th>
                                    <th style="text-align: right;"><?php echo $cat; ?></th>
									<th style="text-align: right;"><?php echo $cft; ?></th>
                                    <th style="text-align: right;"><?php echo $dat; ?></th>
                                    <th style="text-align: right;"><?php echo $dft; ?></th>
                                </tr>
                                
                                <tr>
                                    <th colspan="8">
                                        <div id="currentBalanceContainer" class="label-warning">
                                            <div id="fineGoldContent">
                                            <label>Current Fine:</label>
                                            <span id="getfine">
                                                <?php
                                                    $tf = $cft - $dft;
													if ($tf > 0)
													{
														echo $tf."    Cr";
													}	
													else if ($tf < 0)
													{
														echo abs($tf)."    Dr";
													}
													else
													{
														echo $tf;
													}
                                                ?>
                                            </span>
                                        </div>
                                        <div id="amountContent">
                                            <label>Current Amount:</label>
                                            <span id="getamt">
                                                <?php
                                                    $ta = $cat - $dat;
													if ($ta > 0)
													{
														echo $ta."    Cr";
													}	
													else if ($ta < 0)
													{
														echo abs($ta)."    Dr";
													}
													else
													{
														echo $ta;	
													}
                                                ?>
                                            </span>
                                        </div>
                                        </div>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'assets/js/'; ?>jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<link href="<?php echo $baseUrl.'assets/css/jQueryUI/'; ?>jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">

var dataArray = <?php echo $dataArray; ?>;
$('#party_id').autocomplete({
    source: dataArray,
    autoFocus: true,
    select: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    focus: function (event, ui) {
        $(this).val(ui.item.label);
        $("#hidden_party_id").val(ui.item.value);
        return false;
    },
    minChars: 1
}).autocomplete()._renderItem = function( ul, item ) {
    return $( "<li>" )
    .append( "<a>" + item.label + "</a>" )
    .appendTo( ul );
};;

</script>

</body>
</html>