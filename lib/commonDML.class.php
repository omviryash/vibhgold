<?php 
  class CommonDML
  {
    public $db;
    public $conn;
    function __construct()
    {
        $this->db = new Database();
        $this->conn = $this->db->connect_db();
    }
    
    public function simpleInsert($table,$data)
    {
      $ssKey='';
      $ssVal='';
      foreach($data as $key=>$val)
      {
        $ssKey.=$key.",";
        if($key=='password')
          $ssVal.="'".addslashes(md5($val))."',";
        else
          $ssVal.="'".addslashes($val)."',";
      }
      $ssKey=substr($ssKey,0,-1);
      $ssVal=substr($ssVal,0,-1);
      $sql="insert into ".$table." (".$ssKey.") values (".$ssVal.")";
      mysqli_query($this->conn,$sql);
      return mysqli_insert_id();
    }
    
    public function simpleUpdate($table,$data,$where) { 
      $ssVal = '';
      foreach($data as $key=>$val) {
        if($key=='password')
          $ssVal.=$key." = '".addslashes(md5($val))."',";
        else
          $ssVal.=$key." = '".addslashes($val)."',";
      }
      $ssVal=substr($ssVal,0,-1);
      $sql="update ".$table." set ".$ssVal." where ".$where;
			mysqli_query($this->conn,$sql);
    }
    
    public function selectWithNestedKey($table,$fields,$where,$orderby,$order) {
      $getArray=array();
      $i = 0;
      $sql = "SELECT * FROM ".$table;
      if($where!='') {
        $sql .= " WHERE ".$where;
      }
      if($orderby!='') {
        $sql.=" ORDER BY ".$orderby." ".$order;
      }
      $sqlRes=mysqli_query($this->conn,$sql);
      if(mysqli_num_rows($sqlRes)>0) {
        while($sqlRow=mysqli_fetch_array($sqlRes)) {
          foreach($fields as $key=>$val) {
            $getArray[$i][$val]=$sqlRow[$val];
          }
          $i++;
        }
      }
      return $getArray;
    }
  }
  $dml = new CommonDML();
?>
