<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

global $dml;

$frDate = (isset($_POST['fromDate'])) ? $_POST['fromDate'] : date('d');
$frMonth = (isset($_POST['fromMonth'])) ? $_POST['fromMonth'] : date('m');
$frYear = (isset($_POST['fromYear'])) ? $_POST['fromYear'] : date('Y');

$toDt = (isset($_POST['toDate'])) ? $_POST['toDate'] : date('d');
$toMnth = (isset($_POST['toMonth'])) ? $_POST['toMonth'] : date('m');
$toYr = (isset($_POST['toYear'])) ? $_POST['toYear'] : date('Y');

$fromDate = date('Y-m-d');
$toDate = date('Y-m-d');

if(isset($_POST['fromDate'])){
    $fromDate = $_POST['fromYear'] . '-' . $_POST['fromMonth'] . '-' . $_POST['fromDate'];
    $toDate = $_POST['toYear'] . '-' . $_POST['toMonth'] . '-' . $_POST['toDate'];
}

$paymentQuery = "SELECT p.party_id, pr.name, p.payment_date, p.fine_gold, p.amount, p.comments
                FROM payment p
                LEFT JOIN party pr ON p.party_id = pr.party_id
                WHERE p.payment_date >= '" . $fromDate . "' AND p.payment_date <= '" . $toDate . "'";
$paymentQueryResult = mysqli_query($dml->conn, $paymentQuery);

include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>

<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Payment Detail</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <?php include_once('msg.php');?>
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <form action="" method="post">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>From Date</label>
                                    <div class="input-group">
                                        <select name="fromDate" id="currentDate" class="form-group pull-left">
                                            <?php for($i=1;$i<=31;$i++){?>
                                                <?php if($i < 10){ $i = '0'.$i;} ?>
                                                <?php if($frDate == $i) { ?>
                                                    <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php } ?>
                                            <?php }?>
                                        </select>
                                        <select name="fromMonth" id="currentMonth" class="form-group pull-left">
                                          <?php for($i=1;$i<=12;$i++){?>
                                              <?php if($i < 10){ $i = '0'.$i;}?>
                                              <?php if($frMonth == $i) { ?>
                                                  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                              <?php } else { ?>
                                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                              <?php } ?>
                                          <?php }?>
                                        </select>
                                        <select name="fromYear" id="currentYear" class="form-group pull-left">
                                          <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                              <?php if($frYear == $i) { ?>
                                                  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                              <?php } else { ?>
                                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                              <?php } ?>
                                          <?php }?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <label>To Date</label>
                                    <div class="input-group">
                                        <select name="toDate" id="currentDate" class="form-group pull-left">
                                            <?php for($i=1;$i<=31;$i++){?>
                                                <?php if($i < 10){ $i = '0'.$i;} ?>
                                                <?php if($toDt == $i) { ?>
                                                    <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php } ?>
                                            <?php }?>
                                        </select>
                                        <select name="toMonth" id="currentMonth" class="form-group pull-left">
                                          <?php for($i=1;$i<=12;$i++){?>
                                              <?php if($i < 10){ $i = '0'.$i;}?>
                                              <?php if($toMnth == $i) { ?>
                                                  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                              <?php } else { ?>
                                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                              <?php } ?>
                                          <?php }?>
                                        </select>
                                        <select name="toYear" id="currentYear" class="form-group pull-left">
                                          <?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
                                              <?php if($toYr == $i) { ?>
                                                  <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                              <?php } else { ?>
                                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                              <?php } ?>
                                          <?php }?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <label>&nbsp;</label>
                                    <div class="form-group">
                                        <input type="submit" name="show" value="Show" class="btn btn-primary btn-flat"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                           
							
                    <!-- Display Table-->
                    <form action="" method="post">
                        <div class="box-body">
                            <table id="paymentListTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>Pay.Date</th>
                                        <th>Party</th>
                                        <th>Fine Gold</th>
                                        <th>Amount</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $tot_fine_gold = $tot_amount = 0; ?>
                                    <?php if(mysqli_num_rows($paymentQueryResult) > 0) { ?>
                                        <?php
                                        while ($paymentList = mysqli_fetch_assoc($paymentQueryResult)) {
                                            $tot_fine_gold = $tot_fine_gold + number_format($paymentList['fine_gold'], 3, '.' , '');
                                            $tot_amount = $tot_amount + number_format($paymentList['amount'], 2, '.' , '');
                                        ?>
                                            <tr>
                                                <td><input type="checkbox" name="chkdel[]"/></td>
                                                <td><a href="#">Edit</a> | <a href="#">Delete</a></td>
                                                <td><?php echo $fun->date_ymd_to_dmy($paymentList['payment_date']); ?></td>
                                                <td><?php echo $paymentList['name']; ?></td>
                                                <td align="right">
                                                    <?php echo number_format($paymentList['fine_gold'], 3, '.' , ''); ?>
                                                </td>
                                                <td align="right">
                                                    <?php echo number_format($paymentList['amount'], 2, '.' , ''); ?>
                                                </td>
                                                <td>
                                                    <?php echo $paymentList['comments']; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    
                                    <?php } else { ?>
                                    <tr>
                                        <td colspan="7"><span class="alert-danger">No records found from given dates.</span></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr class="oddRow">
                                        <td colspan="3"><a href="javascript:;" id="selectAll">Select All</a> / <a href="javascript:;" id="clearAll">Clear All</a></td>
                                        <td><strong>Total</strong></td>
                                        <td align="right"><strong><?php echo number_format($tot_fine_gold, 3, '.' , ''); ?></strong></td>
                                        <td align="right"><strong><?php echo number_format($tot_amount, 2, '.' , ''); ?></strong></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer">
                            <input type="submit" name="delete" value="Delete" class="btn btn-primary"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
    
<script type="text/javascript">
$('#selectAll').click(function(){
    $('#paymentListTable').find('input[name^="chkdel"]').parent().addClass('checked').attr({'aria-checked' : 'true'});
});

$('#clearAll').click(function(){
    $('#paymentListTable').find('input[name^="chkdel"]').parent().removeClass('checked').attr({'aria-checked' : 'false'});
});
</script>

</body>
</html>