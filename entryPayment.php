<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['ok'])) {
    unset($_POST['ok']);

	$account_id1 		= $_POST['account_id1'];
	$account_id2 		= $_POST['account_id2'];	
	$amount 			= $_POST['amount'];
	$current_amountcrdr = $_POST['current_amountcrdr'];
	$gold 				= $_POST['gold'];
	$touch 				= $_POST['touch'];
	$fine 				= $_POST['fine'];
	$fine_crdr 			= $_POST['fine_crdr'];
	$gold_rate 			= $_POST['gold_rate'];
	$note 				= $_POST['note'];
	$future_fine 		= $_POST['future_fine'];
	$future_buy_sell 	= $_POST['future_buy_sell'];
	$parity 			= $_POST['parity'];
	$future_amount 		= $_POST['future_amount'];
	$future_amountcrdr 	= $_POST['future_amountcrdr'];
	$timestamp 			= date('Y-m-d H:i:s');
	$current_date1 		= $_POST['currentYear'] . '-' . $_POST['currentMonth'] . '-' . $_POST['currentDate'];

	unset($_POST['currentYear']);
    unset($_POST['currentMonth']);
    unset($_POST['currentDate']);

	if($account_id1 != "" && $account_id2 != "" && $current_date1 != "" && ($amount != "" || $fine != "" )) {
		if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { // Updating the transaction record
			$timestamp = date('Y-m-d H:i:s');
			$sSQL = "UPDATE transaction_master SET account_id1 = '$account_id1',account_id2 = '$account_id2',current_date1='$current_date1',amount='$amount',amount_crdr='$current_amountcrdr',gold='$gold',touch = '$touch',fine='$fine',fine_crdr='$fine_crdr',gold_rate = '$gold_rate', note = '$note', updated_at='$timestamp' WHERE transaction_id = '".$_GET['id']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:entryPayment.php");
			exit;
		} else {
			// insert code for all text boxes in transaction_master table
			$sSQL = "INSERT INTO transaction_master (account_id1, account_id2, current_date1, amount,amount_crdr, gold,touch, fine,fine_crdr, gold_rate, note,
													 created_at, updated_at)
											 VALUES ('$account_id1', '$account_id2', '$current_date1', '$amount', '$current_amountcrdr', '$gold', '$touch', '$fine', 
											          '$fine_crdr', '$gold_rate', '$note', '$timestamp', '$timestamp')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		} 
	} else {
		$_SESSION['error']="Fill Detail Propely For Transaction Ignore If You Dont Want To Add Transaction";
	}
	$timestamp   = date('Y-m-d H:i:s');
	$future_date = $_POST['futureYear'] . '-' . $_POST['futureMonth'] . '-' . $_POST['futureDate'];
	unset($_POST['futureYear']);
    unset($_POST['futureMonth']);
    unset($_POST['futureDate']);

	if($account_id1 != "" && $future_date != "" && ( $future_fine ||  $future_amount )) {
		if(isset($_GET['mode']) && isset($_GET['fid']) && $_GET['mode']==1)	{
			$timestamp 	= date('Y-m-d H:i:s');
			$sSQL 		= "UPDATE future SET account_id = '$account_id1',future_date = '$future_date',future_fine='$future_fine',future_buy_sell='$future_buy_sell',parity='$parity',future_amount='$future_amount',future_amountcrdr = '$future_amountcrdr',updated_at='$timestamp' WHERE future_id = '".$_GET['fid']."'";
			mysqli_query($dml->conn, $sSQL) or print mysqli_error($dml->conn);
			$_SESSION['success']="Record is updated successfully.";
			header("Location:entryPayment.php");
			exit;
		} else {
			// insert code for all text boxes in future table
			$sSQL = "INSERT INTO future (account_id,future_date,future_fine,future_buy_sell,parity,future_amount,future_amountcrdr,created_at,updated_at)
							     VALUES ('$account_id1','$future_date','$future_fine','$future_buy_sell','$parity','$future_amount','$future_amountcrdr','$timestamp','$timestamp')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		}
	} else {
		$_SESSION['error']="Fill Detail Propely For Future Ignore If You Dont Want To Add Future Account Selection Is Necessary To Add Future";
	}
	mysqli_close($conn);
    header("Location:entryPayment.php");
    exit;
}

if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master where transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}

if(isset($_GET['mode']) && isset($_GET['fid'])) {
	if($_GET['mode']==1) {
		$sSQL1 = "SELECT * FROM future where future_id=".$_GET['fid'];

		$rs11 = mysqli_query($dml->conn, $sSQL1) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs11) > 0) {
			$row11 = mysqli_fetch_assoc($rs11);
		}
	} else if($_GET['mode']==2) {
		$sSQL = "DELETE FROM future WHERE future_id=".$_GET['fid'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}


include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Transaction</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
			
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">Add Transaction</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-4">
										<label>Account</label>
										<select class="form-control" name="account_id1" id="account_id1">
											<option value="">Select Account</option>
											<?php  	$sSQL = "SELECT * from account where account_status = 'A' ORDER BY first_name";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option onclick="getaccount(this.value);" value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id1']) && $row1['account_id1']==$row['account_id']){?>selected="selected"<?php } if(isset($row11['account_id']) && $row11['account_id']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-4">
										<label>Opposite Account</label>
										<select class="form-control" name="account_id2" id="account_id2">
											<option value="">Select Account</option>
												<?php  	$sSQL = "SELECT * from account where account_status = 'A' ORDER BY first_name";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option onclick="getoppsiteaccount(this.value);" value="<?php echo $row['account_id']; ?>" <?php if(isset($row1['account_id2']) && $row1['account_id2']==$row['account_id']){?>selected="selected"<?php } ?>><?php echo $row['first_name']; ?></option>
											<?php } ?>
										</select>
									</div>
                                  <!--
									<div class="form-group col-md-4">
										<label>Current Balance:</label>
										<br/>
										<div id="currentBalanceContainer" class="label-warning">
											<div id="fineGoldContent">
												<label>Fine Gold (Grm):</label>
												<span id="getfine">100 Cr</span>
											</div>
											<div id="amountContent">
												<label>Amount (Rs.):</label>
												<span id="getamt">
												50 Cr
												</span>
											</div>
										</div>
									</div>
                                  -->
								</div>
                              <div class="row">
                                <div class="form-group col-md-4 " id="postaccount">
										
								</div>
                                
                                <div class="form-group col-md-4" id="postoppsiteaccount">
										
								</div>
                              </div>
								<div class="row">
									<div class="form-group col-lg-12">
										<?php
											if(isset($row1['current_date1']))
											{
												$orderdate = explode('-', $row1['current_date1']);
												$year = $orderdate[0];
												$month   = $orderdate[1];
												$day  = $orderdate[2];
											}
										?>
										<label>Date </label>
										<div class="input-group">
											<select name="currentDate" id="currentDate" class="form-group pull-left">
												<?php for($i=1;$i<=31;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;} ?>
													<?php if(isset($row1['current_date1']) && $i == $day)
													{
														?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
														<?php
													break;}
													else if(date('d') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											</select>
											<select name="currentMonth" id="currentMonth" class="form-group pull-left">
												<?php for($i=1;$i<=12;$i++){?>
													<?php if($i < 10){ $i = '0'.$i;}?>
													<?php if(isset($row1['current_date1']) && $i == $day)
													{
														?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
														<?php
													break;}
													else if(date('m') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>
											  <select name="currentYear" id="currentYear" class="form-group pull-left">
												<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
													<?php if(isset($row1['current_date1']) && $i == $day)
													{
														?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
														<?php
													break;}
													else if(date('Y') == $i) { ?>
														<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
													<?php } else { ?>
														<option value="<?php echo $i;?>"><?php echo $i;?></option>
													<?php } ?>
												<?php }?>
											  </select>

										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Amount</label>
										<input type="text" name="amount" id="amount" class="form-control" <?php if(isset($row1['amount'])){?>value="<?php echo $row1['amount'];?>" <?php } ?>>
									</div>
									<div class="form-group col-lg-6">
										<label>Debit / Credit</label>
										<select class="form-control" name="current_amountcrdr" id="current_amountcrdr">
											<option value="DR" <?php if(isset($row1['current_amountcrdr']) && $row1['current_amountcrdr']=='DR'){?>selected="selected"<?php } ?>>Debit</option>
											<option value="CR" <?php if(isset($row1['current_amountcrdr']) && $row1['current_amountcrdr']=='CR'){?>selected="selected"<?php } ?>>Credit</option>
										</select> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-3">
										<label>Gold</label>
										<input type="text" name="gold" id="gold" class="form-control" <?php if(isset($row1['gold'])){?>value="<?php echo $row1['gold'];?>" <?php } else { ?> value="150" <?php }  ?>>
									</div>
									<div class="form-group col-lg-3">
										<label>Touch</label>
										<input type="text" name="touch" id="touch" class="form-control" <?php if(isset($row1['touch'])){?>value="<?php echo $row1['touch'];?>" <?php } else { ?> value="100" <?php } ?>>
									</div>
									<div class="form-group col-lg-3">
										<label>Fine</label>
										<input type="text" name="fine" id="fine" class="form-control" <?php if(isset($row1['fine'])){?>value="<?php echo $row1['fine'];?>" <?php } else { ?> value="150" <?php }  ?>>
									</div>
									<div class="form-group col-lg-3">
										<label>Buy/Sell</label>
										<select class="form-control" name="fine_crdr" id="fine_crdr">
											<option value="DR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='DR'){?>selected="selected"<?php } ?>>Buy</option>
											<option value="CR" <?php if(isset($row1['fine_crdr']) && $row1['fine_crdr']=='CR'){?>selected="selected"<?php } ?>>Sell</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Gold Rate</label>
										<input type="text" name="gold_rate" id="gold_rate" class="form-control" <?php if(isset($row1['gold_rate'])){?>value="<?php echo $row1['gold_rate'];?>" <?php } ?>>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-6">
										<label>Note</label>
										<input type="text" name="note" id="note" class="form-control" <?php if(isset($row1['note'])){?>value="<?php echo $row1['note'];?>" <?php } ?>>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
								<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='entryPayment.php'"/>						
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">

$("#gold, #touch, #fine").change(function(){
	var gold  = $("#gold").val();
	var touch = $("#touch").val();

	set_fine = parseFloat((gold * touch / 100).toFixed(3));
	$("#fine").val(set_fine);	
});

$('#fine_gold').focusout(function(){

    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(3) );
    } else {
        $(this).val( parseFloat(0).toFixed(3) );
	}
});

$('#amount').focusout(function() {
    if($(this).val() != 0) {
        $(this).val( parseFloat($(this).val()).toFixed(2) );
	} else {
        $(this).val( parseFloat(0).toFixed(2) );
	}
});

function getaccount(id) {
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}

function getoppsiteaccount(id){
    var xmlhttp;
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			document.getElementById("postoppsiteaccount").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET","ajax_getcurrentbalance.php?id="+id,true);
    xmlhttp.send();
}

</script>
</body>
</html>