<?php
include_once("lib/db.class.php");
include_once("lib/commonDML.class.php");
include_once("lib/commonFunction.php");

if(isset($_POST['ok'])) {
    unset($_POST['ok']);
	
	$item_name = $_POST['item_name'];
	$item_type = $_POST['item_type'];
	$item_type_new = $_POST['item_type_new'];
	$category = $_POST['category'];
	$category_new = $_POST['category_new'];
	if(isset($_POST['special']))
	{
		$special = $_POST['special'];
	}
	else
	{
		$special = 'N';
	}
	if($item_type == "")
	{
		if($item_type_new != "")
		{
			$item_type = $item_type_new;
			$sSQL = "INSERT INTO item_type (item_type) VALUES ('$item_type')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$sSQL = "select item_type_id from item_type where item_type = '$item_type'";
			$rs_item = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
			if(mysqli_num_rows($rs_item) > 0)
			{
				$row1_item = mysqli_fetch_array($rs_item);
			}
			$item_type = $row1_item['item_type_id'];
			$_SESSION['success']="Record is inserted.";
		}
	}
	if($category == "")
	{
		if($category_new != "")
		{
			$category = $category_new;
			$sSQL = "INSERT INTO category (category) VALUES ('$category')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$sSQL = "select category_id from category where category = '$category'";
			$rs_category = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
			if(mysqli_num_rows($rs_category) > 0)
			{
				$row1_category = mysqli_fetch_array($rs_category);
			}
			$category = $row1_category['category_id'];
			$_SESSION['success']="Record is inserted.";
		}
	}
	if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1) { 
		// Updating the transaction record
	} else {
			// insert code for all text boxes in item table
					
			$sSQL = "INSERT INTO item (item_name,item_type_id,category_id,is_special)
											 VALUES ('$item_name','$item_type','$category','$special')";
			if(mysqli_query($dml->conn, $sSQL)) {
				echo "New record created successfully";
			} else {
				echo mysqli_error($dml->conn);
				die;
			}
			$_SESSION['success']="Record is inserted.";
		} 
	mysqli_close($conn);
    header("Location:itemMaster.php");
    exit;
}

/*if(isset($_GET['mode']) && isset($_GET['id'])) {
	if($_GET['mode']==1) {
		$sSQL = "SELECT * FROM transaction_master where transaction_id=".$_GET['id'];
		$rs1  = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		if(mysqli_num_rows($rs1) > 0) {
			$row1 = mysqli_fetch_assoc($rs1);
		}
	} else if($_GET['mode'] == 2) {
		$sSQL = "DELETE FROM transaction_master WHERE transaction_id=".$_GET['id'];
		$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:entryPayment.php");exit;
	}
}*/
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side strech">                
    <!-- Content Header (Page header) -->
	<section class="content-header">
        <h1>Item Master</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- left column -->
        <?php include_once('msg.php');?>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header">
								<h3 class="box-title">Add Item Master Entry</h3>
						</div>
						<!-- general form elements -->
						<!-- form start -->
						<form action="" method="post">
							<div class="box-body">
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Item Name</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="item_name" id="item_name" class="form-control"> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Item Type</label>
									</div>
									<div class="form-group col-lg-3">
										<select class="form-control" name="item_type" id="item_type">
											<option value="">Select Item Type</option>
											<?php  	$sSQL = "SELECT * from item_type ORDER BY item_type";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option value="<?php echo $row['item_type_id']; ?>"><?php echo $row['item_type']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-2">
										<center>OR</center>
									</div>
									<div class="form-group col-lg-2">
										<label>Create New</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="item_type_new" id="item_type_new" class="form-control"> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Category</label>
									</div>
									<div class="form-group col-lg-3">
										<select class="form-control" name="category" id="category">
											<option value="">Select Item Category</option>
											<?php  	$sSQL = "SELECT * from category ORDER BY category";
													$rs1 = mysqli_query($dml->conn, $sSQL) or print(mysqli_error($dml->conn));
													while($row=mysqli_fetch_assoc($rs1))
													{ ?>												
                                            <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-lg-2">
										<center>OR</center>
									</div>
									<div class="form-group col-lg-2">
										<label>Create New</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="text" name="category_new" id="category_new" class="form-control"> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Is Special ?</label>
									</div>
									<div class="form-group col-lg-3">
										<input type="checkbox" name="special" id="special" class="form-control" value="Y"> 
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Opening Stock</label>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<label>Gross Weight (Gram)</label>
									</div>
									<div class="form-group col-lg-2">
										<label>Net Weight (Gram)</label>
									</div>
									<div class="form-group col-lg-2">
										<label>Touch</label>
									</div>
									<div class="form-group col-lg-2">
										<label>Fine Weight (Gram)</label>
									</div>
									<div class="form-group col-lg-2">
										<label>CR/DR</label>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-lg-2">
										<input type="text" name="gross_weight" id="gross_weight" class="form-control"> 
									</div>
									<div class="form-group col-lg-2">
										<input type="text" name="net_weight" id="net_weight" class="form-control">
									</div>
									<div class="form-group col-lg-2">
										<input type="text" name="touch" id="touch" class="form-control">
									</div>
									<div class="form-group col-lg-2">
										<input type="text" name="fine_weight" id="fine_weight" class="form-control">
									</div>
									<div class="form-group col-lg-2">
										<select class="form-control" name="crdr" id="crdr">
											<option value="CR">Credit</option>
											<option value="DR">Debit</option>
										</select> 
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="submit" name="ok" value="Ok" class="btn btn-primary"/>
								<input type="button" name="reset" value="Reset" class="btn btn-primary" onClick="document.location.href='itemMaster.php'"/>						
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>	
    </section><!-- /.content -->
</aside><!-- /.right-side -->
<?php include_once('includes/jsfiles.php'); ?>
</body>
</html>